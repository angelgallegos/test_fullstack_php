Prueba Propiedades
===============

Repositorio con la aplicación de la prueba solicitada por propiedades.com.

# Requerimientos:

-PHP >=5

-MySql

-Composer

# Instalación:

1. Clonar el repositorio. ̣̣
2. Actualizar los requerimientos de composer `composer update` 
3. Cambiar la configuración de conexion a BD en app/config/parameter.yml
4. Una google maps server key es necesaria, para agregarla se puede poner en el archivo: app/config/parameter.yml con el identificador app.googleMapKey
5. Actualizar esquema de base de datos con:
    `php bin/console doctrine:schema:update --force`
6. Existen algunos fixtures para poder probar, se insertan con: `php bin/console doctrine:fixtures:load --append`
7. Ejecutar el servidor con:
   `php bin/console server:run`

# Uso

La pagina principal contiene el buscador de construcciones:

`http://127.0.0.1:8000`

Al cual se puede acceder sin estar autenticado.

Las construcciones creadas por los fixtures se encuentran cercanas a la dirección del metro eugenia.

Las imagenes mostradas sobre el pin de la ubicacion son un enlace que lleva a la página de la construcción. 


El resto de la acciones requieren un usuario con un rol de admin

Si se ejecutaron los fixtures existira uno con el email:

 - usuario.administrador@propiedades.com


Y la contraseña:

 - password
 
En el resto del proyecto se podra:

- Crear ubicaciones

- Crear construcciones

- Crear caracteristicas

- Asociar imagenes a las construcciones

- Asociar caracteristicas a las construcciones

