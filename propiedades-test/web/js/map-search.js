$(document).ready(function(){
    var mapContainer = $("#map-container");
    var map;
    var mapOptions = {
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    };
    //var isDraggable = $(document).width() > 480 ? true : false;
    map = new google.maps.Map(mapContainer[0], mapOptions);
    if(!!navigator.geolocation) {    
	navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            map.setCenter(pos);
        }, function(){
            handleLocationError();
        });
    } else {
        handleLocationError();
    }
    function search(){
        var bounds = map.getBounds();
        var center = bounds.getCenter();
        var ne = bounds.getNorthEast();
        var marker;
        $.ajax({
            type: "GET",
            url: "/search",
            data: {
                lat: center.lat(),
                lng: center.lng(),
                neLat: ne.lat(),
                neLng: ne.lng()
            },
            dataType: "json",
            success: function(response) {
                $( ".building-id" ).remove();
                $.each(response.result_set, function(i, item) {
                    $("#container").append("<input type='hidden' class='building-id' id='building-"+item.id+"' value='"+item.id+"'>");
                    var position = new google.maps.LatLng(item.latitude, item.longitude);
                    bounds.extend(position);
                    marker = createSlideMarker("hc1", item.id, {
                        position: position,
                        map : map
                    });
                    map.fitBounds(bounds);
                });
            }
        });

        var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
            this.setZoom(14);
            google.maps.event.removeListener(boundsListener);
        });
    }
    $("#searchOnMap").on("click", function(){
        search();
    });    
});
function handleLocationError(){
    console.log("Error");
}
function createSlideMarker(id, buildingId, opts) {
    var slideDiv = document.getElementById(id);
    var marker = new google.maps.Marker(opts);
    var infoWnd = new google.maps.InfoWindow({
        content : slideDiv
    });
    google.maps.event.addListener(marker, "click", function(){
        slideDiv.style.visibility = "visible";
        var bId = $("#building-"+buildingId).val();
        $.ajax({
            type: "GET",
            url: "/building/"+bId,
            dataType: "json",
            success: function(response) {
                console.log(response)
                $("#hc1 #titulo").html(response.name);
                if($.isEmptyObject(response.images)){
                    $("#image-0").attr("src","http://downloadicons.net/sites/default/files/office-building-icon-68724.png");
                    $("#image-1").attr("src","http://downloadicons.net/sites/default/files/office-building-icon-68724.png");
                    $("#image-2").attr("src","http://downloadicons.net/sites/default/files/office-building-icon-68724.png");
                    $("#link-0").attr("href","/building/"+bId);
                    $("#link-1").attr("href","/building/"+bId);
                    $("#link-2").attr("href","/building/"+bId);
                }else{
                    $.each(response.images, function(i, item) {
                        $("#image-"+i).attr("src","/buildingimage/"+item.id);
                    })
                }
            }
        });
        infoWnd.open(marker.getMap(), marker);
    });
    google.maps.event.addListener(infoWnd, "domready", function(){
    });
    google.maps.event.addListener(infoWnd, "closeclick", function(){
        slideDiv.style.visibility = "hidden";
    });
    return marker;
}



