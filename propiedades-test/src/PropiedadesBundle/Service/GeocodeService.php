<?php
namespace PropiedadesBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface; 
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

/**
 * GeocodeService
 *
 * @author Angel Gallegos
 */
class GeocodeService{
    
    private $googleMapKey;

    private $googleMapUrlService;
    
    private $container;
    /**
     * @var $client \GuzzleHttp\Client|null
     */
    private $clientGoogle;
    
    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $this->googleMapKey = $this->container->getParameter('app.googleMapKey');
        $this->googleMapUrlService = "https://maps.googleapis.com/maps/api/geocode/json";
        
        $this->clientGoogle = new Client([
            'base_uri' => $this->googleMapUrlService
        ]);
    }
    public function geoCodePlaceId($lat, $lng){
        $query = [
            'key'      => $this->googleMapKey,
            'latlng' => $lat.",".$lng,
            'language' => 'es',
        ];

        $result = $this->clientGoogle->get($this->googleMapUrlService, [
            'query' => $query
        ]);

        $content = json_decode($result->getBody()->getContents(), true);
        
        $addressData = $content['results'][0];

        $components = $this->setAddress($addressData, $lat, $lng);
        
        return $components;
    }
    private function setAddress($addressData, $lat, $lng){
        $components = [];
        foreach ($addressData['address_components'] as $component) {
            if (in_array('route', $component['types'])) {
                $components['street'] = $component['long_name'];
            }else if(!in_array('route', $component['types']) && (!isset($components['street']) || $components['street']=="N/P" )){
                $components['street'] = "N/P";
            }
            
            if (in_array('street_number', $component['types'])) {
                $components['streetNumber'] = $component['long_name'];
            }else if(!in_array('street_number', $component['types']) && (!isset($components['streetNumber']) || $components['streetNumber']=="N/P" )){
                $components['streetNumber'] = "N/P";
            }
            
            if (in_array('sublocality', $component['types'])) {
                $components['neighborhood'] = $component['long_name'];
            }else if(!in_array('sublocality', $component['types']) && (!isset($components['neighborhood']) || $components['neighborhood']=="N/P" )){
                $components['neighborhood'] = "N/P";
            }
            
            if (in_array('neighborhood', $component['types'])) {
                $components['neighborhood'] = $component['long_name'];
            }else if(!in_array('neighborhood', $component['types']) && (!isset($components['neighborhood']) || $components['neighborhood']=="N/P" )){
                $components['neighborhood'] = "N/P";
            }

            if (in_array('administrative_area_level_1', $component['types'])) {
                $components['state'] = $component['long_name'];
            }
                
            if (in_array('locality', $component['types'])) {
                $components['posible_city'][] = $component['long_name'];
            }
            
            if (in_array('administrative_area_level_2', $component['types'])) {
                $components['posible_city'][] = $component['long_name'];
            }
            
            if (in_array('administrative_area_level_3', $component['types'])) {
                $components['posible_city'][] = $component['long_name'];
            }
            
            if (in_array('administrative_area_level_4', $component['types'])) {
                $components['posible_city'][] = $component['long_name'];
            }
            
            if (in_array('administrative_area_level_5', $component['types'])) {
                $components['posible_city'][] = $component['long_name'];
            }

            if (in_array('postal_code', $component['types'])) {
                $components['zipCode'] = $component['long_name'];
            }else if(!in_array('postal_code', $component['types']) && (!isset($components['zipCode']) || $components['zipCode']=="N/P" )){
                $components['zipCode'] = "N/P";
            }
        }
        $components['latitude'] = $lat;
        $components['longitude'] = $lng;
        return $components;
    }
}
