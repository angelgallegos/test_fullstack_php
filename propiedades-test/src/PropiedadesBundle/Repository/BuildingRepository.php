<?php

namespace PropiedadesBundle\Repository;

/**
 * BuildingRepository
 *
 * @author Angel Gallegos
 * 
 */
class BuildingRepository extends \Doctrine\ORM\EntityRepository{
    public function searchNear($lat, $lng, $distance){
        $em = $this->getEntityManager();
        $query = $em->createQuery('SELECT b.id, b.keyword, b.name, l.street, l.streetNumber, '
                . 'l.latitude, l.longitude, '
                . '(6378.7 * acos(sin(radians(:lat)) *  sin(radians(l.latitude)) + cos(radians(:lat)) * cos(radians(l.latitude)) * cos(radians(l.longitude) - radians(:lng)))) AS distance '
                . 'FROM PropiedadesBundle:Building b '
                . 'JOIN b.location l '
                . 'HAVING distance < :distance');
        $query->setParameters(array(
            'lat' => $lat,
            'lng' => $lng,
            'distance'=>$distance
        ));
        $buildings = $query->getResult();
        return $buildings;
    }
}
