<?php

namespace PropiedadesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class BuildingType extends AbstractType{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
                ->add('keyword', TextType::class, array('label' => 'Clave'))
                ->add('name', TextType::class, array('label' => 'Nombre'))
                ->add('complement', TextType::class, array('label' => 'Complemento', "required"=>false))
                ->add('location', EntityType::class, array(
                        'class' => 'PropiedadesBundle:Location',
                        'label' => 'Ubicación'
                    )
                 );
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'PropiedadesBundle\Entity\Building'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(){
        return 'propiedadesbundle_building';
    }


}
