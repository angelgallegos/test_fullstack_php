<?php

namespace PropiedadesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Vich\UploaderBundle\Form\Type\VichImageType;

class BuildingImageType extends AbstractType{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
                ->add('imageFile', VichImageType::class, [
                        'required' => false,
                        'allow_delete' => true,
                        'download_label' => '...',
                        'download_uri' => true,
                        'image_uri' => true
                 ])
                ->add('imageSize')
                ->add('updatedAt')
                ->add('building');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'PropiedadesBundle\Entity\BuildingImage'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(){
        return 'propiedadesbundle_buildingimage';
    }


}
