<?php

namespace PropiedadesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class LocationType extends AbstractType{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
                ->add('street', TextType::class, array('label' => 'Calle'))
                ->add('streetNumber', TextType::class, array('label' => 'Numero'))
                ->add('zipCode', TextType::class, array('label' => 'Codigo Postal'))
                ->add('reference', TextType::class, array('label' => 'Referencia', 'required' => false))
                ->add('neighborhood', TextType::class, array('label' => 'Colonia'))
                ->add('latitude', NumberType::class, array(
                                'scale' => 9,
                                'label' => 'Latitud',
                                'required' => true)
                 )
                ->add('longitude', NumberType::class, array(
                                'scale' => 9,
                                'label' => 'Longitud',
                                'required' => true))
                ->add('city', EntityType::class, array(
                        'class' => 'PropiedadesBundle:City',
                        'label' => 'Ciudad'
                    ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(array(
            'data_class' => 'PropiedadesBundle\Entity\Location',
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(){
        return 'propiedadesbundle_location';
    }


}
