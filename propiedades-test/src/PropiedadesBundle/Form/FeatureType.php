<?php

namespace PropiedadesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FeatureType extends AbstractType{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
                ->add('name',  TextType::class, array('label' => 'Nombre'))
                ->add('description', TextType::class, array('label' => 'Description'))
                ->add('type', ChoiceType::class, array(
                    'choices'  => array(
                        "Apartamento"=>'apartament', "Edificio"=>'building', "Ubicación"=>'location'
                        ),
                    'label'=>'Tipo de caracteristica'
                    )
                );
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PropiedadesBundle\Entity\Feature'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'propiedadesbundle_feature';
    }


}
