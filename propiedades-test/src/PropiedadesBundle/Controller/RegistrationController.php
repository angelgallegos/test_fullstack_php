<?php

namespace PropiedadesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use PropiedadesBundle\Entity\Users;
use PropiedadesBundle\Form\UsersType;


/**
 * RegisterController
 * 
 * @author Angel Gallegos
 */
class RegistrationController extends Controller{
    
    /**
     * @Route("/register", name="register")
     */
    public function registerAction(Request $request){
        $user = new Users();
        $form = $this->createForm(UsersType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $encoder = $this->get('security.password_encoder');
            $password = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $user->setRole('ROLE_USER');

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('login');
        }

        return $this->render('PropiedadesBundle:auth:register.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
