<?php

namespace PropiedadesBundle\Controller;

use PropiedadesBundle\Entity\BuildingFeature;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Buildingfeature controller.
 *
 * @Route("buildingfeature")
 */
class BuildingFeatureController extends Controller{
    /**
     * Lists all buildingFeature entities.
     *
     * @Route("/", name="buildingfeature_index")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction(){
        $em = $this->getDoctrine()->getManager();

        $buildingFeatures = $em->getRepository('PropiedadesBundle:BuildingFeature')->findAll();

        return $this->render('PropiedadesBundle:buildingfeature:index.html.twig', array(
            'buildingFeatures' => $buildingFeatures,
        ));
    }
    
    /**
     * Creates a new building entity.
     *
     * @Route("/new/{id}", name="buildingfeature_new")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction(Request $request, $id){
        $buildingFeature = new BuildingFeature();
        $repository = $this->getDoctrine()->getRepository('PropiedadesBundle:Building');
        $building = $repository->find($id);
        
        $buildingFeature->setBuilding($building);
        $form = $this->createForm('PropiedadesBundle\Form\BuildingFeatureType', $buildingFeature);
        return $this->render('PropiedadesBundle:buildingfeature:new.html.twig', array(
            'building' => $building,
            'form' => $form->createView(),
        ));
    }
    
    /**
     * Creates a new building entity.
     *
     * @Route("/create", name="buildingfeature_create")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function createAction(Request $request){
        $buildingFeature = new BuildingFeature();
        $form = $this->createForm('PropiedadesBundle\Form\BuildingFeatureType', $buildingFeature);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($buildingFeature);
            $em->flush();
            if($request->isXmlHttpRequest()) {
                return new JsonResponse(array("status"=>"success", "message"=>"Caracteristica creada"), Response::HTTP_CREATED);
            }else{
                return $this->redirectToRoute('buildingfeature_show', array('id' => $buildingFeature->getId()));
            }
            
        }else{
            if($request->isXmlHttpRequest()) {
                $errors= array();
                foreach ($form->getErrors(true) as $key => $error) {
                    $errors[$key] = $error->getMessage();
                } 
                return new JsonResponse(array("status"=>"error", "message"=>"Errores al agregar la caracteristica", "errors"=>$errors), Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }
        return $this->render('PropiedadesBundle:buildingfeature:new.html.twig', array(
            'building' => $buildingFeature,
           'form' => $form->createView(),
        ));
        
        
    }

    /**
     * Finds and displays a buildingFeature entity.
     *
     * @Route("/{id}", name="buildingfeature_show")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function showAction(BuildingFeature $buildingFeature){

        return $this->render('PropiedadesBundle:buildingfeature:show.html.twig', array(
            'buildingFeature' => $buildingFeature,
        ));
    }
}
