<?php

namespace PropiedadesBundle\Controller;

use PropiedadesBundle\Entity\Location;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Location controller.
 *
 * @author Angel Gallegos
 * 
 * @Route("location")
 */
class LocationController extends Controller{
    /**
     * Lists all location entities.
     *
     * @Route("/", name="location_index")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction(){
        $em = $this->getDoctrine()->getManager();

        $locations = $em->getRepository('PropiedadesBundle:Location')->findAll();

        return $this->render('PropiedadesBundle:location:index.html.twig', array(
            'locations' => $locations,
        ));
    }
    
    /**
     * Creates a new location entity.
     *
     * @Route("/search", name="location_search")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function searchAction(Request $request){
        $lat = $request->get('lat');
        $lng = $request->get('lng');
        $geocoder = $this->get('geocoder');
        $geoCodePlace = $geocoder->geoCodePlaceId($lat, $lng);
        foreach($geoCodePlace["posible_city"] as $pCity){
            $city = $this->getDoctrine()->getRepository('PropiedadesBundle:City')->findByName($pCity);
            if($city != null){
                $geoCodePlace["city"] = $city->getId();
                break;
            }
        }
        $resp = array("status"=>"success", "message" => "Ubicacion creada", "data" => $geoCodePlace);
        if(!isset($geoCodePlace["city"])){
            $state = $this->getDoctrine()->getRepository('PropiedadesBundle:State')->findByName($geoCodePlace["state"]);
            if($state !== null){
                $resp["cities"] = $state->getCities();
            }else{
                $states = $this->getDoctrine()->getRepository('PropiedadesBundle:State')->getAll();
                $resp["states"] = $states;
            }
        }
        return new JsonResponse($resp, Response::HTTP_OK);
    }
    
    /**
     * Creates a new location entity.
     *
     * @Route("/new", name="location_new")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction(Request $request){
        $location = new Location();
        $form = $this->createForm('PropiedadesBundle\Form\LocationType', $location);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($location);
            $em->flush();
            if($request->isXmlHttpRequest()) {
                return new JsonResponse(array("status"=>"success", "message"=>"Ubicación creada", 'id' => $location->getId()), Response::HTTP_CREATED);
            }else{
                return $this->redirectToRoute('location_show', array('id' => $location->getId()));
            }
            
        }else{
            if($request->isXmlHttpRequest()) {
                $errors= array();
                foreach ($form->getErrors(true) as $key => $error) {
                    $errors[$key] = $error->getMessage();
                } 
                return new JsonResponse(array("status"=>"error", "message"=>"Errores al agregar la ubicación", "errors"=>$errors), Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        return $this->render('PropiedadesBundle:location:new.html.twig', array(
            'location' => $location,
            'form' => $form->createView(),
        ));
    }
    

    /**
     * Finds and displays a location entity.
     *
     * @Route("/{id}", name="location_show")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function showAction(Location $location){
        $deleteForm = $this->createDeleteForm($location);

        return $this->render('PropiedadesBundle:location:show.html.twig', array(
            'location' => $location,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing location entity.
     *
     * @Route("/{id}/edit", name="location_edit")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAction(Request $request, Location $location){
        $deleteForm = $this->createDeleteForm($location);
        $editForm = $this->createForm('PropiedadesBundle\Form\LocationType', $location);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('location_edit', array('id' => $location->getId()));
        }

        return $this->render('PropiedadesBundle:location:edit.html.twig', array(
            'location' => $location,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a location entity.
     *
     * @Route("/{id}", name="location_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, Location $location){
        $form = $this->createDeleteForm($location);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($location);
            $em->flush();
        }

        return $this->redirectToRoute('location_index');
    }

    /**
     * Creates a form to delete a location entity.
     *
     * @param Location $location The location entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Location $location){
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('location_delete', array('id' => $location->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
