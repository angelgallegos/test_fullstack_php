<?php
namespace PropiedadesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Location controller.
 *
 * @author Angel Gallegos
 * 
 * @Route("search")
 */
class SearchController extends Controller{
    /**
     * Lists all location entities.
     *
     * @Route("/", name="search_index")
     * @Method("GET")
     */
    public function indexAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $lat = $request->query->get('lat');
        $lng = $request->query->get('lng');
        $neLat = $request->query->get('neLat');
        $neLng = $request->query->get('neLng');
        $buildings = $em->getRepository('PropiedadesBundle:Building')->searchNear($lat, $lng, $this->calculateDistance($lat, $lng, $neLat, $neLng));
        if ($buildings === null) {
            return new JsonResponse(array('success' => 'fail', 'message'=>'No se han encontrado resultados'), Response::HTTP_NOT_FOUND);
        }
        return new JsonResponse(array('status' => 'success', 'message'=>'Lista de construcciones encontradas', 'result_set'=>$buildings), Response::HTTP_OK);
    }
    private function calculateDistance($lat, $lng, $neLat, $neLng){
        $r = 6378.7;
        $lat = deg2rad($lat);
        $lng = deg2rad($lng);
        $neLat = deg2rad($neLat);
        $neLng = deg2rad($neLng);
        $dist = $r * acos(sin($lat) * sin($neLat) + cos($lat) * cos($neLat) * cos($neLng - $lng));
        return $dist;
    }

    
}
