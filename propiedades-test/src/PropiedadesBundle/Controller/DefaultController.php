<?php

namespace PropiedadesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller{
    
    /**
    * @Route("/", name="welcome")
    */
    public function homeAction(Request $request){
        return $this->render('PropiedadesBundle:Default:home.html.twig');
    }
}
