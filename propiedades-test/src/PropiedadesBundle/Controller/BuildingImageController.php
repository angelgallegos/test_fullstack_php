<?php

namespace PropiedadesBundle\Controller;

use PropiedadesBundle\Entity\BuildingImage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;



/**
 * Buildingimage controller.
 *
 * @Route("buildingimage")
 */
class BuildingImageController extends Controller{
    /**
     * Lists all buildingImage entities.
     *
     * @Route("/", name="buildingimage_index")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction(){
        $em = $this->getDoctrine()->getManager();

        $buildingImages = $em->getRepository('PropiedadesBundle:BuildingImage')->findAll();

        return $this->render('buildingimage/index.html.twig', array(
            'buildingImages' => $buildingImages,
        ));
    }

    /**
     * Creates a new buildingImage entity.
     *
     * @Route("/new", name="buildingimage_new")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function newAction(Request $request){
        $buildingImage = new Buildingimage();
        $repository = $this->getDoctrine()->getRepository('PropiedadesBundle:Building');
        $id = $request->query->get('id');
        $building = $repository->find($id);
        $buildingImage->setBuilding($building);
        $form = $this->createForm('PropiedadesBundle\Form\BuildingImageType', $buildingImage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($buildingImage);
            $em->flush();

            return $this->redirectToRoute('buildingimage_show', array('id' => $buildingImage->getId()));
        }

        return $this->render('buildingimage/new.html.twig', array(
            'buildingImage' => $buildingImage,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a buildingImage entity.
     *
     * @Route("/{id}", name="buildingimage_show")
     * @Method("GET")
     */
    public function showAction(BuildingImage $buildingImage){
        $baseDir = $this->get('kernel')->getRootDir() . "/../web/images/buildings/";
        $file = $baseDir.$buildingImage->getImageName();
        $response = new BinaryFileResponse($file);
        return $response;
    }

    /**
     * Displays a form to edit an existing buildingImage entity.
     *
     * @Route("/{id}/edit", name="buildingimage_edit")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function editAction(Request $request, BuildingImage $buildingImage){
        $deleteForm = $this->createDeleteForm($buildingImage);
        $editForm = $this->createForm('PropiedadesBundle\Form\BuildingImageType', $buildingImage);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('buildingimage_edit', array('id' => $buildingImage->getId()));
        }

        return $this->render('buildingimage/edit.html.twig', array(
            'buildingImage' => $buildingImage,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a buildingImage entity.
     *
     * @Route("/{id}", name="buildingimage_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction(Request $request, BuildingImage $buildingImage){
        $form = $this->createDeleteForm($buildingImage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($buildingImage);
            $em->flush();
        }

        return $this->redirectToRoute('buildingimage_index');
    }

    /**
     * Creates a form to delete a buildingImage entity.
     *
     * @param BuildingImage $buildingImage The buildingImage entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(BuildingImage $buildingImage){
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('buildingimage_delete', array('id' => $buildingImage->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
