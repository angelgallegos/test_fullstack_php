<?php

namespace PropiedadesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * City
 *
 * @author Angel Gallegos
 * 
 * @ORM\Table(name="city")
 * @ORM\Entity(repositoryClass="PropiedadesBundle\Repository\CityRepository")
 */
class City{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @ORM\ManyToOne(targetEntity="State", inversedBy="cities")
     * @ORM\JoinColumn(name="state_id", referencedColumnName="id")
     */
    private $state;

    /**
     * @ORM\OneToMany(targetEntity="Location", mappedBy="city")
     */
    private $locations;

    public function __construct() {
        $this->locations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return City
     */
    public function setName($name){
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(){
        return $this->name;
    }
    
    /**
     * Set state
     * 
     * @param \PropiedadesBundle\Entity\State $state 
     * 
     * @return City
     */
    public function setState(\PropiedadesBundle\Entity\State $state){
        $this->state = $state;
        return $this;
    }
    
    /**
     * Get state
     * 
     * @return \PropiedadesBundle\Entity\State
     */
    public function getState(){
        return $this->state;
    }
    
    public function __toString() {
        return $this->name;
    }
}

