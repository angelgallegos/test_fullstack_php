<?php

namespace PropiedadesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * BuildingImage
 *
 * @author Angel Gallegos
 * 
 * @ORM\Table(name="building_image")
 * @ORM\Entity(repositoryClass="PropiedadesBundle\Repository\BuildingImageRepository")
 * @Vich\Uploadable
 */
class BuildingImage{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Building", inversedBy="images")
     * @ORM\JoinColumn(name="building_id", referencedColumnName="id")
     */
    private $building;
    
     /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="building_image", fileNameProperty="imageName", size="imageSize")
     * 
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $imageSize;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Product
     */
    public function setImageFile(File $image = null){
        $this->imageFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTimeImmutable();
        }
        
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile(){
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     *
     * @return BuildingImage
     */
    public function setImageName($imageName){
        $this->imageName = $imageName;
        
        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName(){
        return $this->imageName;
    }
    
    /**
     * @param integer $imageSize
     *
     * @return Product
     */
    public function setImageSize($imageSize){
        $this->imageSize = $imageSize;
        
        return $this;
    }

    /**
     * @return integer|null
     */
    public function getImageSize(){
        return $this->imageSize;
    }

    
    /**
     * Set building
     * 
     * @param \PropiedadesBundle\Entity\Building $building 
     * 
     * @return BuildingImage
     */
    public function setBuilding(\PropiedadesBundle\Entity\Building $building){
        $this->building = $building;
        return $this;
    }
    
    /**
     * Get building
     * 
     * @return \PropiedadesBundle\Entity\Building
     */
    public function getBuilding(){
        return $this->building;
    }
    
    public function setUpdatedAt($updatedAt){
        $this->updatedAt = $updatedAt;
        return $this;
    }
    
    public function getUpdatedAt(){
        return $this->updatedAt;
    }
}

