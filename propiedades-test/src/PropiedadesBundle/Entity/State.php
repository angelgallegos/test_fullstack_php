<?php

namespace PropiedadesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * State
 *
 * @author Angel Gallegos
 * 
 * @ORM\Table(name="state")
 * @ORM\Entity(repositoryClass="PropiedadesBundle\Repository\StateRepository")
 */
class State{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * 
     * State code in compliance with ISO 3166-2
     * 
     * @ORM\Column(name="code", type="string", length=6, unique=true)
     * @Assert\Regex(
     *     pattern="/^(MX-)[A-Z]{3}$/",
     *     match=true,
     *     message="El codigo de estado no cumple el formato"
     * )
     */
    private $code;

    /**
     * @ORM\OneToMany(targetEntity="City", mappedBy="state")
     */
    private $cities;
    
    public function __construct() {
        $this->cities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return State
     */
    public function setName($name){
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return State
     */
    public function setCode($code){
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode(){
        return $this->code;
    }
    
    /**
     * Get cities
     * 
     * @return ArrayCollection
     */
    public function getCities(){
        return $this->cities;
    }

    public function __toString() {
        return $this->name;
    }
}

