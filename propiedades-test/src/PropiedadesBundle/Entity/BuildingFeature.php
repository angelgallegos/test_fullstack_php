<?php

namespace PropiedadesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BuildingFeature
 *
 * @author Angel Gallegos
 * 
 * @ORM\Table(name="building_feature")
 * @ORM\Entity(repositoryClass="PropiedadesBundle\Repository\BuildingFeatureRepository")
 * @UniqueEntity(
 *   fields={"building", "feature"},
 *   errorPath="port",
 *   message="Esta carasteristica ya ha sido registrada para esta construccion"
 * )
 */
class BuildingFeature{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer", nullable=true)
     * @Assert\GreaterThanOrEqual(
     * value = 1,
     * message="El valor debe de ser mayor que {{ compared_value }}"
     * )
     * @Assert\NotBlank()
     */
    private $amount;
    
    /** 
     * @ORM\ManyToOne(targetEntity="Building", inversedBy="features") 
     * @ORM\JoinColumn(name="building_id", referencedColumnName="id", nullable=false) 
     */
    protected $building;

    /** 
     * @ORM\ManyToOne(targetEntity="Feature", inversedBy="buildings") 
     * @ORM\JoinColumn(name="feature_id", referencedColumnName="id", nullable=false) 
     */
    protected $feature;

    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return BuildingFeature
     */
    public function setAmount($amount){
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount(){
        return $this->amount;
    }
    
    /**
     * Set feature
     *
     * @param \PropiedadesBundle\Entity\Feature $feature
     *
     * @return BuildingFeature
     */
    public function setFeature(\PropiedadesBundle\Entity\Feature $feature){
        $this->feature = $feature;

        return $this;
    }

    /**
     * Get feature
     *
     * @return \PropiedadesBundle\Entity\Feature
     */
    public function getFeature(){
        return $this->feature;
    }
    
    /**
     * Set building
     *
     * @param \PropiedadesBundle\Entity\Building $building
     *
     * @return BuildingFeature
     */
    public function setBuilding(\PropiedadesBundle\Entity\Building $building){
        $this->building = $building;

        return $this;
    }

    /**
     * Get building
     *
     * @return \PropiedadesBundle\Entity\Building
     */
    public function getBuilding(){
        return $this->building;
    }
}

