<?php

namespace PropiedadesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Role
 *
 * @author Angel Gallegos
 * 
 * @ORM\Table(name="role")
 * @ORM\Entity(repositoryClass="PropiedadesBundle\Repository\RoleRepository")
 */
class Role{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="authority", type="string", length=100, unique=true)
     */
    private $authority;

    /**
    * 
    * @ORM\ManyToMany(targetEntity="Users", mappedBy="roles")
    */
    private $users;
    
    public function __construct() {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set authority
     *
     * @param string $authority
     *
     * @return Role
     */
    public function setAuthority($authority){
        $this->authority = $authority;

        return $this;
    }

    /**
     * Get authority
     *
     * @return string
     */
    public function getAuthority(){
        return $this->authority;
    }
    
    /**
    * Add users
    *
    * @param \PropiedadesBundle\Entity\Users $users
    * 
    * @return Role
    */
    public function addUsers(\PropiedadesBundle\Entity\Users $users){
        if (!$this->users->contains($users)){
            $this->users[] = $users;
            $users->addRole($this);
        }

        return $this;
    }
}

