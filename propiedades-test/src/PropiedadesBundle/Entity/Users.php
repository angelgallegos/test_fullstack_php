<?php

namespace PropiedadesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use PropiedadesBundle\Entity\Role;

/**
 * Users
 * 
 * @author Angel Gallegos
 * 
 * @ORM\Table(name="Users")
 * @ORM\Entity
 * @UniqueEntity(fields="email", message="Este e-mail ya ha sido usado")
 */
class Users implements UserInterface{
    
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="FirstName", type="string", length=255, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="LastName", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="Email", type="string", length=100, nullable=false, unique=true)
     * @Assert\Email(
     *     message = "El email: '{{ value }}' no es valido.",
     * )
     */
    private $email;
    
    /**
     * @Assert\Length(max=4096)
     */
    protected $plainPassword;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Password", type="string", length=255, nullable=true)
     */
    private $password;
    
    /**
     * 
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     * @ORM\JoinTable(name="user_role",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="ID")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    private $roles;
    
    /**
     * @var string
     */
    private $fullName;
    
    public function __construct() {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId(){
        return $this->id;
    }
    
    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Users
     */
    public function setFirstName($firstName){
        $this->firstName = $firstName;
        return $this;
    }
    /**
     * Get firstName
     * 
     * @return string
     */
    public function getFirstName(){
        return $this->firstName;
    }
    
    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Users
     */
    public function setLastName($lastName){
        $this->lastName = $lastName;
        return $this;
    }
    /**
     * Get lastName
     * 
     * @return string
     */
    public function getLastName(){
        return $this->lastName;
    }
    
    /**
     * Set email
     *
     * @param string $email
     *
     * @return Users
     */
    public function setEmail($email){
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     * 
     * @return string
     */
    public function getEmail(){
        return $this->email;
    }
    
    /**
     * Get email
     * 
     * @return string
     */
    public function getUsername(){
        return $this->email;
    }

    /**
     * Set plainPassword
     * 
     * @param string $plainPassword
     * 
     * @return Users
     */
    public function setPlainPassword($plainPassword){
        $this->plainPassword = $plainPassword;
        return $this;
    }

    /**
     * Get plainPassword
     * 
     * @return string
     */
    public function getPlainPassword(){
        return $this->plainPassword;
    }

    /**
     * Set password
     * 
     * @param string $password
     * 
     * @return Users
     */
    public function setPassword($password){
        $this->password = $password;
    }

    /**
     * Get password
     * 
     * @return string
     */
    public function getPassword(){
        return $this->password;
    }

    /**
     * Set roles
     *
     * @param \PropiedadesBundle\Entity\Role $roles
     *
     * @return Users
     */
    public function setRoles(\PropiedadesBundle\Entity\Role $roles = null){
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get roles
     *
     * @return \PropiedadesBundle\Entity\Role
     */
    public function getRoles(){
        $roles = [];
        foreach($this->roles as $role){
            array_push($roles, $role->getAuthority());
        }
        return $roles;
    }
    
    /**
     * Set fullName
     * 
     * @param string $fullName
     * 
     * @return Users
     */
    public function setFullName($fullName){
        $this->fullName = $fullName;
        return $this;
    }
    
    /**
     * Get fullName
     * 
     * @return string
     */
    public function getFullName(){
        return $this->firstName." ".$this->lastName;
    }
    
    /**
    * Add roles
    *
    * @param \PropiedadesBundle\Entity\Role $roles
    * 
    * @return Users
    */
    public function addRole(\PropiedadesBundle\Entity\Role $roles){
        if (!$this->roles->contains($roles)){
            $this->roles[] = $roles;
            $roles->addUsers($this);
        }

        return $this;
    }
    
    /**
     * Get salt
     * 
     * @return null 
     */
    public function getSalt(){
        return null;
    }
    
    /**
     * Erase credentials
     * 
     * @return null
     */
    public function eraseCredentials(){
        return null;
    }

}

