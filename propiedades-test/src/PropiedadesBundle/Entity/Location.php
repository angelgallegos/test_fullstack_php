<?php

namespace PropiedadesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Location
 * 
 * @author Angel Gallegos
 *
 * @ORM\Table(name="location")
 * @ORM\Entity(repositoryClass="PropiedadesBundle\Repository\LocationRepository")
 */
class Location{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="streetNumber", type="string", length=50)
     */
    private $streetNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="zipCode", type="string", length=32)
     * @Assert\Regex(
     *  pattern="/^\d{4,5}$/",
     *  match=true,
     *  message="El codigo postal no es valido"
     * )
     */
    private $zipCode;
    
    /**
     * @var string
     *
     * @ORM\Column(name="neighborhood", type="string", length=255, nullable=true)
     */
    private $neighborhood;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, nullable=true)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="decimal", precision=13, scale=9)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="decimal", precision=13, scale=9)
     */
    private $longitude;

    /**
     * @ORM\ManyToOne(targetEntity="City", inversedBy="locations")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    private $city;

    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Location
     */
    public function setStreet($street){
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet(){
        return $this->street;
    }

    /**
     * Set streetNumber
     *
     * @param string $streetNumber
     *
     * @return Location
     */
    public function setStreetNumber($streetNumber){
        $this->streetNumber = $streetNumber;

        return $this;
    }

    /**
     * Get streetNumber
     *
     * @return string
     */
    public function getStreetNumber(){
        return $this->streetNumber;
    }

    /**
     * Set zipCode
     *
     * @param string $zipCode
     *
     * @return Location
     */
    public function setZipCode($zipCode){
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string
     */
    public function getZipCode(){
        return $this->zipCode;
    }

    /**
     * Set neighborhood
     *
     * @param string $neighborhood
     *
     * @return Location
     */
    public function setNeighborhood($neighborhood){
        $this->neighborhood = $neighborhood;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getNeighborhood(){
        return $this->neighborhood;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Location
     */
    public function setReference($reference){
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference(){
        return $this->reference;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return Location
     */
    public function setLatitude($latitude){
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude(){
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return Location
     */
    public function setLongitude($longitude){
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude(){
        return $this->longitude;
    }
    
    /**
     * Set city
     * 
     * @param  \PropiedadesBundle\Entity\City $city
     * 
     * @return Location
     */
    public function setCity(\PropiedadesBundle\Entity\City $city){
        $this->city = $city;
        return $this;
    }
    
    /**
     * Get city
     * 
     * @return \PropiedadesBundle\Entity\City
     */
    public function getCity(){
        return $this->city;
    }
    
    public function __toString() {
        return $this->street." ".$this->streetNumber." ".$this->neighborhood;
    }
}

