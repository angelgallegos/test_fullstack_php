<?php

namespace PropiedadesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Building
 *
 * @author Angel Gallegos
 * 
 * @ORM\Table(name="building")
 * @ORM\Entity(repositoryClass="PropiedadesBundle\Repository\BuildingRepository")
 */
class Building{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="keyword", type="string", length=11, unique=true)
     * @Assert\Regex(
     *  pattern="/^(PCOM-)[A-Z]{3}\/[0-9]{2}$/",
     *  match=true,
     *  message="La clave de la construccion no coincide con el formato requerido"
     * )
     */
    private $keyword;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\Regex(
     *  pattern="/^[a-zA-Z0-9 ]+$/",
     *  match=true,
     *  message="El nombre de la construcción debe ser un valor alfanumerico"
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="complement", type="string", length=255, nullable=true)
     */
    private $complement;

    /** 
     * @ORM\OneToMany(targetEntity="BuildingFeature", mappedBy="building") 
     */
    protected $features;
    
    /**
     * @ORM\ManyToOne(targetEntity="Location")
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id")
     */
    private $location;
    
    /**
     * @ORM\OneToMany(targetEntity="BuildingImage", mappedBy="building")
     */
    private $images;
    
    public function __construct() {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
        $this->features = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set keyword
     *
     * @param string $keyword
     *
     * @return Building
     */
    public function setKeyword($keyword){
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * Get keyword
     *
     * @return string
     */
    public function getKeyword(){
        return $this->keyword;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Building
     */
    public function setName($name){
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * Set complement
     *
     * @param string $complement
     *
     * @return Building
     */
    public function setComplement($complement){
        $this->complement = $complement;

        return $this;
    }

    /**
     * Get complement
     *
     * @return string
     */
    public function getComplement(){
        return $this->complement;
    }

    /**
     * Set location
     *
     * @param \PropiedadesBundle\Entity\Location $location
     *
     * @return Building
     */
    public function setLocation(\PropiedadesBundle\Entity\Location $location){
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return \PropiedadesBundle\Entity\Location
     */
    public function getLocation(){
        return $this->location;
    }
    
    /**
     * Get features 
     * 
     * @return ArrayCollection
     */
    public function getFeatures(){
        return $this->features;
    }
    
    /**
     * Get images 
     * 
     * @return ArrayCollection
     */
    public function getImages(){
        return $this->images;
    }

    public function __toString() {
        return $this->name;
    }
}

