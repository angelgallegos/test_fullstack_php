<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use PropiedadesBundle\Entity\Role;

/**
 * RolesFixtures
 *
 * @author Angel Gallegos
 */
class RolesFixtures extends Fixture{
    public function load(ObjectManager $manager){
        $roleUser = new Role();
        $roleUser->setAuthority('ROLE_USER');
        $manager->persist($roleUser);
        
        $roleAdmin = new Role();
        $roleAdmin->setAuthority('ROLE_ADMIN');
        $manager->persist($roleAdmin);
        
        $roleManager = new Role();
        $roleManager->setAuthority('ROLE_MANAGER');
        $manager->persist($roleManager);
        
        $manager->flush();
        
        $this->addReference('role-admin', $roleAdmin);
        $this->addReference('role-manager', $roleManager);
        
    }
}
