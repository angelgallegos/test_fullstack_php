<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use PropiedadesBundle\Entity\State;

/**
 * StatesFixtures
 *
 * @author Angel Gallegos
 */
class StatesFixtures extends Fixture{
    
    public function load(ObjectManager $manager){
        $states = [
            ['name'=>'Aguascalientes', 'code'=>'MX-AGU'],
            ['name'=>'Baja California', 'code'=>'MX-BCN'],
            ['name'=>'Baja California Sur', 'code'=>'MX-BCS'],
            ['name'=>'Campeche', 'code'=>'MX-CAM'],
            ['name'=>'Coahuila', 'code'=>'MX-COA'],
            ['name'=>'Colima', 'code'=>'MX-COL'],
            ['name'=>'Chiapas', 'code'=>'MX-CHP'],
            ['name'=>'Chihuahua', 'code'=>'MX-CHH'],
            ['name'=>'Ciudad de México', 'code'=>'MX-CMX'],
            ['name'=>'Durango', 'code'=>'MX-DUR'],
            ['name'=>'Guanajuato', 'code'=>'MX-GUA'],
            ['name'=>'Guerrero', 'code'=>'MX-GRO'],
            ['name'=>'Hidalgo', 'code'=>'MX-HID'],
            ['name'=>'Jalisco', 'code'=>'MX-JAL'],
            ['name'=>'México', 'code'=>'MX-MEX'],
            ['name'=>'Michoacán', 'code'=>'MX-MIC'],
            ['name'=>'Morelos', 'code'=>'MX-MOR'],
            ['name'=>'Nayarit', 'code'=>'MX-NAY'],
            ['name'=>'Nuevo León', 'code'=>'MX-NLE'],
            ['name'=>'Oaxaca', 'code'=>'MX-OAX'],
            ['name'=>'Puebla', 'code'=>'MX-PUE'],
            ['name'=>'Querétaro', 'code'=>'MX-QUE'],
            ['name'=>'Quintana Roo', 'code'=>'MX-ROO'],
            ['name'=>'San Luis Potosí', 'code'=>'MX-SLP'],
            ['name'=>'Sinaloa', 'code'=>'MX-SIN'],
            ['name'=>'Sonora', 'code'=>'MX-SON'],
            ['name'=>'Tabasco', 'code'=>'MX-TAB'],
            ['name'=>'Tamaulipas', 'code'=>'MX-TAM'],
            ['name'=>'Tlaxcala', 'code'=>'MX-TLA'],
            ['name'=>'Veracruz', 'code'=>'MX-VER'],
            ['name'=>'Yucatán', 'code'=>'MX-YUC'],
            ['name'=>'Zacatecas', 'code'=>'MX-ZAC']
        ];
        foreach ($states as $st){
            $state = new State();
            $state->setName($st['name']);
            $state->setCode($st['code']);
            $manager->persist($state);
            $this->addReference('state-'.$st['code'], $state);
        }
        $manager->flush();

    }
}
