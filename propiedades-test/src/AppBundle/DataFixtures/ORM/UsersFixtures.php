<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use PropiedadesBundle\Entity\Users;

/**
 * UsersFixtures
 *
 * @author Angel Gallegos
 */
class UsersFixtures extends Fixture{
    public function load(ObjectManager $manager){
        $user1 = new Users();
        $user1->setEmail('usuario.manager@propiedades.com');
        $user1->setFirstName('Cosme');
        $user1->setLastName('Fulanito');
        $encoder = $this->container->get('security.password_encoder');
        $passwordUser1 = $encoder->encodePassword($user1, 'password');
        $user1->setPassword($passwordUser1);
        $user1->addRole($this->getReference('role-manager'));
        
        $user2 = new Users();
        $user2->setEmail('usuario.administrador@propiedades.com');
        $user2->setFirstName('Cosme');
        $user2->setLastName('Fulanito');
        $passwordUser2 = $encoder->encodePassword($user2, 'password');
        $user2->setPassword($passwordUser2);
        $user1->addRole($this->getReference('role-admin'));

        $manager->persist($user1);
        $manager->persist($user2);
        $manager->flush();
        
    }
    public function getDependencies(){
        return array(
            RolesFixtures::class
        );
    }
    
}
