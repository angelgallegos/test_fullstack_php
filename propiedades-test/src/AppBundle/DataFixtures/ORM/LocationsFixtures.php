<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use PropiedadesBundle\Entity\Location;

/**
 * LocationsFixtures
 *
 * @author Angel Gallegos
 */
class LocationsFixtures extends Fixture{
    public function load(ObjectManager $manager){
        $location1 = new Location();
        $location1->setStreet("Calle Yacatas")->setStreetNumber("424")->setZipCode("03020")->setReference("Entre Concepción Beistegui y Eugenia")
                 ->setNeighborhood("Narvarte")->setCity($this->getReference('city-benito-juarez'))->setLatitude(19.388423)->setLongitude(-99.1556565);
        $location2 = new Location();
        $location2->setStreet("Anaxagoras")->setStreetNumber("791")->setZipCode("03020")->setReference("Entre Concepción Beistegui y Eugenia")
                 ->setNeighborhood("Narvarte")->setCity($this->getReference('city-benito-juarez'))->setLatitude(19.3864913)->setLongitude(-99.1601886);
        $location3 = new Location();
        $location3->setStreet("Calle Torres Adalid")->setStreetNumber("1156")->setZipCode("03020")->setReference("Entre Nicolas San Juan y Heriberto Frias")
                 ->setNeighborhood("Narvarte")->setCity($this->getReference('city-benito-juarez'))->setLatitude(19.3897613)->setLongitude(-99.1619337);
        $location4 = new Location();
        $location4->setStreet("Paseo de la Reforma")->setStreetNumber("313")->setZipCode("06500")->setReference("Entre Rio Nilo y Rio Guadalquivir")
                 ->setNeighborhood("Tabacalera")->setCity($this->getReference('city-cuauhtemoc'))->setLatitude(19.4264264)->setLongitude(-99.1720117);
        
        /*$location->setStreet("Calle 1")->setStreetNumber("1A")->setZipCode("03020")->setReference("Entre algo y algo")
                 ->setCity($this->getReference('city-benito-juarez'))->setLatitude(19.388423)->setLongitude(-99.1556565);
        
        $location->setStreet("Calle 1")->setStreetNumber("1A")->setZipCode("03020")->setReference("Entre algo y algo")
                 ->setCity($this->getReference('city-benito-juarez'))->setLatitude(19.388423)->setLongitude(-99.1556565);
        
        $location->setStreet("Calle 1")->setStreetNumber("1A")->setZipCode("03020")->setReference("Entre algo y algo")
                 ->setCity($this->getReference('city-benito-juarez'))->setLatitude(19.388423)->setLongitude(-99.1556565);*/
        
        /*"SELECT  id,  street, streetNumber, "
         . "(6378.7 *    "
                . "acos("
                        . "cos(radians(19.3862291)) * "
                        . "cos(radians(latitude)) * "
                        . "cos(radians(longitude) - radians(-99.1596364)) + "
                        . "sin(radians(19.3862291)) * "
                        . "sin(radians(latitude))"
                . ")"
         . ") AS distance  FROM location  HAVING distance < 25  ORDER BY distance LIMIT 0, 20";*/
        //3963.0 * arccos[sin(lat1) *  sin(lat2) + cos(lat1) * cos(lat2) * cos(lon2 – lon1)]
        //SELECT id, street, streetNumber, (6378.7 * acos(sin(radians(19.3862291)) *  sin(radians(latitude)) + cos(radians(19.3862291)) * cos(radiand(latitude)) * cos(radians(longitude) - radians(-99.1596364)))) AS distance  FROM location  HAVING distance < 25  ORDER BY distance LIMIT 0, 20
        //3963.0 * arccos[sin(lat1/57.2958) * sin(lat2/57.2958) + cos(lat1/57.2958) * cos(lat2/57.2958) *  cos(lon2/57.2958 -lon1/57.2958)]
        $manager->persist($location1);
        $manager->persist($location2);
        $manager->persist($location3);
        $manager->persist($location4);
        $manager->flush();
    }
    public function getDependencies(){
        return array(
            CitiesFixtures::class,
        );
    }
}
