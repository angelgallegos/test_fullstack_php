<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use PropiedadesBundle\Entity\Feature;
/**
 * FeatureFixtures
 *
 * @author Angel Gallegos
 */
class FeatureFixtures extends Fixture{
    public function load(ObjectManager $manager){
        $features = [
            ['name'=>'Roof Garden','description'=>'Roof Garden', 'type'=>'building'], 
            ['name'=>'Espacio de estacionamiento','description'=>'Espacio de estacionamiento', 'type'=>'building'],
            ['name'=>'Cuarto de servicio','description'=>'Cuarto de servicio', 'type'=>'apartment'],
            ['name'=>'Cuarto de lavado','description'=>'Cuarto de lavado', 'type'=>'building'],
            ['name'=>'Jaula de tendido','description'=>'Jaula de tendido', 'type'=>'apartment'],
            ['name'=>'Piscina','description'=>'Piscina', 'type'=>'building'],
            ['name'=>'Baño','description'=>'Baño', 'type'=>'apartment'],
            ['name'=>'Sala','description'=>'Sala', 'type'=>'apartment'],
            ['name'=>'Comedor','description'=>'Comedor', 'type'=>'apartment'],
            ['name'=>'Recamara','description'=>'Recamara', 'type'=>'apartment']
        ];
        foreach($features as $feat){
            $feature = new Feature();
            $feature->setName($feat['name']);
            $feature->setDescription($feat['description']);
            $feature->setType($feat['type']);
            $manager->persist($feature);
        }
        $manager->flush();
    }
}
